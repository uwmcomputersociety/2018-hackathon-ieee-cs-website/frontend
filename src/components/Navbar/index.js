import React from 'react';
import { Navbar, NavbarBrand, Button } from 'reactstrap';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import './styles.scss';

export default withRouter(props => {
  const loggedIn = props.auth.isAuthenticated();

  let roleLinks = null;
  if (loggedIn) {
    roleLinks = <NavLink to='/personal'>Personal</NavLink>;
  }

  const logInOutButton = (
    <Button onClick={() => props.auth[loggedIn ? 'logout' : 'login']()}>
      Log {loggedIn ? 'Out' : 'In'}
    </Button>
  )

  return (
    <div>
      <Navbar>
        <NavbarBrand tag={NavLink} to='/'>Home</NavbarBrand>
        <NavLink to='/inventory'>Inventory</NavLink>
        {roleLinks}
        {logInOutButton}
      </Navbar>
    </div>
  );
});
