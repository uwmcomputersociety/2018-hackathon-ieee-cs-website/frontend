import React from 'react';
import { Container } from 'reactstrap';

export default props => {
  props.auth.handleAuthentication();
  return (
    <Container>
      Loading...
    </Container>
  );
};
