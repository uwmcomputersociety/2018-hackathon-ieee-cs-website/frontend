import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { Announcements, Callback, ErrorPage, Inventory, Members, Personal } from './containers';
import { Navbar } from './components'
import Auth, { history } from './auth';
import 'bootstrap/dist/css/bootstrap.min.css';
const auth = new Auth();

const AuthRoute = ({ component: Component, path, ...rest }) => (
  <Route exact path={path} render={() => auth.isAuthenticated() ? <Component {...rest} /> : <Redirect to='/' />} />
);

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <React.Fragment>
          <Navbar auth={auth} />
          <Switch>
            <Route exact path='/' render={props => <Announcements auth={auth} {...props} />}  />
            <Route exact path='/inventory' render={props => <Inventory auth={auth} {...props} />} />
            <AuthRoute path='/personal' component={Personal} />
            <AuthRoute path='/members' component={Members} />
            <Route exact path='/callback' render={props => <Callback auth={auth} {...props} />} />
            <Route exact path='/error' component={ErrorPage} />
            <Redirect to='/' />
          </Switch>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
